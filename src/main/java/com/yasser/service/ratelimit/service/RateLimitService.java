package com.yasser.service.ratelimit.service;

import com.yasser.service.ratelimit.dto.RateLimitDto;
import io.github.bucket4j.BucketConfiguration;
import io.github.bucket4j.ConsumptionProbe;
import io.github.bucket4j.mssql.MSSQLSelectForUpdateBasedProxyManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.TimeUnit;

import static java.time.Duration.ofMinutes;

@Service
@Slf4j
public final class RateLimitService {

   private final MSSQLSelectForUpdateBasedProxyManager<String> proxyManager;
   private final BucketConfiguration SMS = BucketConfiguration.builder().addLimit(limit -> limit.capacity(20).refillIntervally(10, ofMinutes(1))).build();
   private final BucketConfiguration EMAIL = BucketConfiguration.builder().addLimit(limit -> limit.capacity(5).refillGreedy(1, ofMinutes(5))).build();

   public RateLimitService(MSSQLSelectForUpdateBasedProxyManager<String> proxyManager) {this.proxyManager = proxyManager;}

   public RateLimitDto tryToSendLoginOtpSms(String username) {
      log.info("request to try to consume send login otp sms : {}", username);

      return buildRateLimitDto(proxyManager.builder().build("SMS" + username, SMS).tryConsumeAndReturnRemaining(1));
   }


   /* use another bucket with same configuration of sms bucket */
   public RateLimitDto tryToSendChangePasswordOtpSms(String username) {
      log.info("request to try to consume send change password otp sms : {}", username);

      return buildRateLimitDto(proxyManager.builder().build("SMS" + username, SMS).tryConsumeAndReturnRemaining(1));
   }

   public boolean isAvailableTokensSms(String username) {
      log.info("request to check available Tokens sms: {}", username);

      return proxyManager.builder().build("SMS_" + username, SMS).getAvailableTokens() > 0L;
   }

   /* use another bucket with same configuration of sms bucket */
   public RateLimitDto tryToSendLoginOtpEmail(String username) {
      log.info("request to try to consume send change password otp email : {}", username);

      return buildRateLimitDto(proxyManager.builder().build("EMAIL" + username, EMAIL).tryConsumeAndReturnRemaining(1));
   }

   private RateLimitDto buildRateLimitDto(ConsumptionProbe consumptionProbe) {

      RateLimitDto bucketDto = new RateLimitDto();

      if (consumptionProbe.isConsumed()) {
         bucketDto.setAvailableTokens(consumptionProbe.getRemainingTokens());
         return bucketDto;
      }

      bucketDto.setReFillMinutes(BigDecimal.valueOf(TimeUnit.NANOSECONDS.toSeconds(consumptionProbe.getNanosToWaitForRefill())).divide(BigDecimal.valueOf(60), 2, RoundingMode.CEILING).multiply(BigDecimal.valueOf(60)).doubleValue());
      bucketDto.setAvailableTokens(0L);
      bucketDto.setErrorMsg("You have exhausted the number of unsuccessful attempts. Please try again later");

      return bucketDto;
   }
}