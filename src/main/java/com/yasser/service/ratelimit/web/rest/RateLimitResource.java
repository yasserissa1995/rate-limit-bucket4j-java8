package com.yasser.service.ratelimit.web.rest;

import com.yasser.service.ratelimit.dto.RateLimitDto;
import com.yasser.service.ratelimit.service.RateLimitService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
public class RateLimitResource {

   private final RateLimitService bucketService;

   @GetMapping("/bucket/send-login-otp-sms")
   public ResponseEntity<RateLimitDto> sendLoginOtpSms() {
      log.info("Rest request to consume send login otp sms");

      return ResponseEntity.ok().body(bucketService.tryToSendLoginOtpSms("12345"));
   }

   // use same configuration 'use a configuration of bucket from another bucket '
   @GetMapping("/bucket/send-change-password-otp-sms")
   public ResponseEntity<RateLimitDto> sendChangePasswordOtpSms() {
      log.info("Rest request to consume change password otp sms");

      return ResponseEntity.ok().body(bucketService.tryToSendChangePasswordOtpSms("111111"));
   }

   @GetMapping("/bucket/send-login-otp-email")
   public ResponseEntity<RateLimitDto> sendOtp() {
      log.info("Rest request to consume send login otp email");

      return ResponseEntity.ok().body(bucketService.tryToSendLoginOtpEmail("4444_yasser"));
   }
}