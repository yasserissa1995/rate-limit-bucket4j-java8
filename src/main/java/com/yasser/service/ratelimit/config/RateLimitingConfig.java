package com.yasser.service.ratelimit.config;

import io.github.bucket4j.distributed.jdbc.BucketTableSettings;
import io.github.bucket4j.distributed.jdbc.PrimaryKeyMapper;
import io.github.bucket4j.distributed.jdbc.SQLProxyConfiguration;
import io.github.bucket4j.mssql.MSSQLSelectForUpdateBasedProxyManager;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@AllArgsConstructor
public class RateLimitingConfig {


   private final DataSource dataSource;

   @Bean
   public MSSQLSelectForUpdateBasedProxyManager<String> proxyManager() {

      SQLProxyConfiguration configuration = SQLProxyConfiguration.builder().withTableSettings(BucketTableSettings.customSettings("TRACKING_RATE_LIMIT", "PK_KEY_NAME", "STATE")).withPrimaryKeyMapper(PrimaryKeyMapper.STRING).build(dataSource);

      return new MSSQLSelectForUpdateBasedProxyManager<>(configuration);
   }
}