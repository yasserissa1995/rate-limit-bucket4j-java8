package com.yasser.service.ratelimit.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RateLimitDto {

   private Boolean consume;
   private Double reFillMinutes;
   private Long capacity;
   private Long availableTokens;
   private String errorMsg;

}